package example

import akka.actor.{ActorRef, FSM, Props}

class Room extends FSM[Room.State, Room.Data] {

  import Room._

  startWith(State.Initial, Data.Initial)

  when(State.Initial) {
    case Event(msg@Command.Subscribe(_, _, _), _) =>
      msg.sender ! Visitor.Message.Out(s"ROOM[${msg.id}]> Welcome, ${msg.name}")
      goto(State.Active) using Data.Active(msg.id, collection.immutable.HashMap(msg.sender -> msg.name))
  }

  when(State.Active) {
    case Event(msg@Command.Subscribe(_, _, _), stateData: Data.Active) =>
      for ((visitor: ActorRef, name: String) <- stateData.visitors) {
        visitor ! Visitor.Message.Out(s"ROOM[${msg.id}] ${msg.name} has joined to room")
      }

      msg.sender ! Visitor.Message.Out(s"ROOM[${stateData.id}] Welcome ${msg.name}")
      stay using stateData.copy(visitors = stateData.visitors + (msg.sender -> msg.name))

    case Event(msg@Command.Message(_, _, message), stateData: Data.Active) =>
      stateData.visitors.get(msg.sender) match {
        case Some(senderName) =>
          for ((visitor, name) <- stateData.visitors if visitor != msg.sender) {
            visitor ! Visitor.Message.Out(s"[$senderName] $message")
          }
        case None =>
      }

      stay using stateData.copy(
        visitors = stateData.visitors - sender()
      )

    case Event(msg@Command.Leave(_, _), stateData: Data.Active) =>
      stateData.visitors.get(msg.sender) match {
        case Some(senderName) =>
          for ((visitor, name) <- stateData.visitors if visitor != msg.sender) {
            visitor ! Visitor.Message.Out(s"ROOM[${stateData.id}] Visitor $senderName left room")
          }
        case None =>
      }
      stay using stateData.copy(
        visitors = stateData.visitors - sender()
      )
  }

  initialize()

}

object Room {
  def props = Props(new Room)

  val idExtractor: PartialFunction[Any, String] = {
    case msg: Command => msg.id
  }

  type Visitors = collection.immutable.HashMap[ActorRef, String]

  sealed trait Command {
    def sender: ActorRef

    def id: String
  }

  object Command {

    final case class Subscribe(sender: ActorRef, id: String, name: String) extends Command

    final case class Leave(sender: ActorRef, id: String) extends Command

    final case class Message(sender: ActorRef, id: String, message: String) extends Command

  }

  sealed trait Data

  object Data {

    case object Initial extends Data

    final case class Active(id: String, visitors: Visitors) extends Data

  }

  sealed trait State

  object State {

    case object Initial extends State

    case object Active extends State

  }

}
