package example

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, Props}
import akka.io.{IO, Tcp}
import akka.io.Tcp.{Bind, Bound, CommandFailed, Connected, Register, SO}

class Server(host: String, port: Int) extends Actor with ActorLogging {
  override def preStart(): Unit = {
    log.info("Starting server")

    import context.system
    val opts = List(SO.KeepAlive(on = true), SO.TcpNoDelay(on = true))
    IO(Tcp) ! Bind(self, new InetSocketAddress(host, port), options = opts)
  }

  override def receive: Receive = {
    case b@Bound(localAddress) => log.info(s"Server bound at $localAddress")
    case CommandFailed(_: Bind) =>
      log.info("Command failed")
      context stop self
    case c@Connected(remote, local) =>
      log.info(s"new incoming connection $remote")
      val visitor = context.actorOf(Visitor.props(sender()))
      visitor ! Visitor.Message.Greeting
      sender() ! Register(visitor)
  }

}

object Server {
  def props(address: String, port: Int) = Props(new Server(address, port))
}
