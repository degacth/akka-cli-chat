package example

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

class RoomSupervisor extends Actor with ActorLogging {
  private val rooms = collection.mutable.HashMap.empty[String, ActorRef]

  override def receive: Receive = {
    case message: Room.Command.Subscribe =>
      rooms.get(message.id) match {
        case Some(room) => room ! message
        case None =>
          val room = context.actorOf(Room.props)
          rooms.put(message.id, room)
          room ! message
      }

    case message =>
      val id = Room.idExtractor(message)
      rooms.get(id) match {
        case Some(room) => room ! message
        case None => log.warning(s"no room for message: $message")
      }
  }

}

object RoomSupervisor {
  def props = Props(new RoomSupervisor)
}
