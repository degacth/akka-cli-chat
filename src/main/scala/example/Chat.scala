package example

import akka.actor.ActorSystem

import scala.io.StdIn

object Chat extends App {
  val port = 2555
  val system = ActorSystem("server")
  val rooms = system.actorOf(RoomSupervisor.props, "rooms")
  val server = system.actorOf(Server.props("localhost", port))

  StdIn.readLine()
  system.terminate()
}