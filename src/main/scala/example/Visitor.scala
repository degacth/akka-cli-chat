package example

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp
import akka.util.ByteString

class Visitor(connection: ActorRef) extends Actor with ActorLogging {

  import Visitor._

  private val rooms = context.actorSelection("akka://server/user/rooms")
  var name: String = _
  var room: String = _

  override def receive: Receive = {
    case Message.Greeting =>
      connection ! encode("Welcome! Enter your name")
    case Tcp.Received(data) =>
      val message = decode(data)
      name = message
      connection ! encode(s"$name enter room name:")
      context.become(chooseRoomState)
  }

  def chooseRoomState: Receive = {
    case Tcp.Received(data) =>
      val message = decode(data)
      room = message
      rooms ! Room.Command.Subscribe(self, room, name)
      sender ! nr
      context.become(inRoomState)
  }

  def inRoomState: Receive = {
    case Tcp.Received(data) =>
      val message = decode(data)
      rooms ! Room.Command.Message(self, room, message)
      sender ! nr

    case Message.Out(message) =>
      log.info(s"message is out ${message}")
      connection ! encode(message)
    case Tcp.PeerClosed =>
      rooms ! Room.Command.Leave(self, room)
      context stop self
    case x => log info s"Visitor unhandled: $x"
  }
}

object Visitor {
  def props(connection: ActorRef) = Props(new Visitor(connection))

  private def decode(bytes: ByteString): String = bytes.decodeString("US-ASCII").trim

  private def encode(msg: String): Tcp.Write = Tcp.Write(ByteString(s"$msg\n > "))

  private def nr: Tcp.Write = Tcp.Write(ByteString(" > "))

  object Message {

    case object Greeting

    final case class Out(message: String)

  }

}
